﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sims2Files
{
    class conversion
    {
        public static string Hex2Ascii(string hexString)
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hexString.Length; i += 2) // i is + 2 because Hex values always have double figures
            {

                if (hexString.Substring(i, 2) != "00")
                {
                    string hs = hexString.Substring(i, 2);
                    sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                else
                {
                    sb.Append("¿"); // ¿ = unknown character 00
                }
            }

            return sb.ToString();
        }
    }
}
