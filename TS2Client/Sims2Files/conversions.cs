﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sims2Files
{
    public class conversions
    {
        public static string Hex2Ascii(string hexString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hexString.Length; i += 2 /* Hex values always come in twos! Therefore it's +2. */)
            {
                if (hexString.Substring(i, 2) != "00")
                {

                    /* The "00" substring is always considered "." in
                     * most Hex editors but we'll go for a simple dash 
                     * instead to avoid confusion (seen below in the 
                     * else statement!). */

                    string hs = hexString.Substring(i, 2);
                    sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                else
                {
                    sb.Append("-");
                }
            }
            return sb.ToString();
        }
    }
}
