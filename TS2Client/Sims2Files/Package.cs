﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Sims2Files;
using Sims2Files.formats;
using Sims2Files.formats.dbpf;
using Sims2Files.utils;


namespace Sims2Files
{
    public class Package : IDisposable
    {
        public string filePath;
        public IoBuffer file_;
        public int numFiles;
        /// <summary>
        /// Declare a new instance of a package file
        /// </summary>
        /// <param name="file">The package file's path</param>
        public Package (String file)
        {
            DBPF pFile = new DBPF(file);
            try
            {
                IoBuffer fileR = new IoBuffer(File.Open(file, FileMode.Open, FileAccess.Read, FileShare.Read));
                filePath = file;
                file_ = fileR;
            }
            catch
            {
                throw new Exception("File could not be opened; is the path valid?");
            }
        }

        /// <summary>
        /// Read a file from the package
        /// </summary>
        /// <param name="filenumber">The number of the file to read</param>
        /// <param name="file">The package file's IoBuffer</param>
        /// <returns></returns>
        public string Read(int filenumber, IoBuffer file)
        {
            int i = 1;
            string toReturn = "";
            DBPF dFile = new DBPF(filePath);
            DBPFEntry dEntry = new DBPFEntry();
            Int64 prevFileStop;
            UInt32 lastPos = 0x00000000;
            file.Seek(SeekOrigin.Begin, 0);
            file.Skip(24);
            UInt32 numEntries = file.ReadUInt32();
            numFiles = (int) numEntries;
            //UInt32 indOffset = file.ReadUInt32();
            //file.Skip(16);
            file.Skip(20);
            UInt32 minVersion = file.ReadUInt32();
            //file.Seek(SeekOrigin.Begin, indOffset);            // Goto the start of the index
            file.Seek(SeekOrigin.Begin, 86);            // Goto the start of the index
            Byte[] fOffset;
            Byte[] fSize;
            while (i <= filenumber)
            {
                file.Seek(SeekOrigin.Begin, lastPos + 86);
                if ((int)minVersion == 1)
                {
                    file.ReadBytes(12);
                    fOffset = file.ReadBytes(2);
                    String temp = BitConverter.ToString(fOffset).Replace("-", "");
                    lastPos = lastPos + UInt32.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);
                    dEntry.FileOffset = 0x00000066 + UInt32.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);
                    //uifile1.FileOffset = fOffset;
                    fSize = file.ReadBytes(2);
                    temp = BitConverter.ToString(fSize).Replace("-", "");
                    lastPos = lastPos + UInt32.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);
                    //uifile1.FileSize = fSize;
                    dEntry.FileSize = UInt32.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);
                    
                    //prevFileStop = BitConverter.ToInt64(fOffset, 0) + BitConverter.ToInt64(fSize, 0);
                }
                else
                {
                    file.ReadBytes(16);
                    fOffset = file.ReadBytes(2);
                    String temp = BitConverter.ToString(fOffset).Replace("-", "");
                    lastPos = lastPos + UInt32.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);
                    dEntry.FileOffset = 0x00000066 + UInt32.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);
                    //uifile1.FileOffset = fOffset;
                    fSize = file.ReadBytes(2);
                    temp = BitConverter.ToString(fSize).Replace("-", "");
                    lastPos = lastPos + UInt32.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);
                    //uifile1.FileSize = fSize;
                    dEntry.FileSize = UInt32.Parse(temp, System.Globalization.NumberStyles.AllowHexSpecifier);

                    //prevFileStop = BitConverter.ToInt64(fOffset, 0) + BitConverter.ToInt64(fSize, 0);
                }
                if (i != filenumber) i++; 
                else break;
            }
            //FAR3.Decompresser dcp = new FAR3.Decompresser();
            //toReturn = Hex2Ascii(BitConverter.ToString(dcp.Decompress(dFile.GetEntry(dEntry))).Replace("-", ""));
            toReturn = Hex2Ascii(BitConverter.ToString(dFile.GetEntry(dEntry)).Replace("-", ""));
            return toReturn;
        }

        /// <summary>
        /// Read a file from the package
        /// </summary>
        /// <param name="fileSize">The size of the file</param>
        /// <param name="fileOffset">The offset of the file</param>
        /// <param name="file">The package file's IoBuffer</param>
        /// <returns></returns>
        public string Read(UInt32 fileSize, UInt32 fileOffset, IoBuffer file)
        {
            string toReturn = "";
            DBPF dFile = new DBPF(filePath);
            DBPFEntry dEntry = new DBPFEntry();
            file.Seek(SeekOrigin.Begin, 0);
            file.Skip(24);
            UInt32 numEntries = file.ReadUInt32();
            file.Skip(20);
            UInt32 minVersion = file.ReadUInt32();
            file.Seek(SeekOrigin.Begin, 86);            // Goto the start of the index
            Byte[] fOffset;
            Byte[] fSize;
            dEntry.FileOffset = fileOffset;
            dEntry.FileSize = fileSize;
            toReturn = Hex2Ascii(BitConverter.ToString(dFile.GetEntry(dEntry)).Replace("-", ""));
            return toReturn;
        }

        /// <summary>
        /// Convert a hex string to an ASCII string.
        /// </summary>
        /// <param name="hexString">The hex string, without separators.</param>
        /// <returns></returns>
        public static string Hex2Ascii(string hexString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hexString.Length; i += 2 /* Hex values always come in twos! Therefore it's +2. */)
            {
                if (hexString.Substring(i, 2) != "00")
                {

                    /* The "00" substring is always considered "." in
                     * most Hex editors but we'll go for a simple dash 
                     * instead to avoid confusion (seen below in the 
                     * else statement!). */

                    string hs = hexString.Substring(i, 2);
                    sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                else
                {
                    sb.Append("-");
                }
            }
            return sb.ToString();
        }

        public void Dispose()
        {
            file_.Dispose();
        }
    }
}
