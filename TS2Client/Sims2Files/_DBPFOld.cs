﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sims2Files
{
    class _DBPFOld
    {
        //#region DBPF Reader
        //    // Here is where we will load the DBPF file's contents and put them into the listbox.
        //    BinaryReader Reader = new BinaryReader(File.Open(txbPath.Text, FileMode.Open));
        //    DBPFEntry Entry = new DBPFEntry();
        //    int i = 0;
        //    string magic = "";
        //    uint majver;
        //    uint minver;
        //    string creationdte = "null";
        //    string modifydte = "null";
        //    uint indmajver;
        //    uint entrycount;
        //    uint offset;
        //    uint size;
        //    uint tindent;
        //    uint tindoff;
        //    uint tindsz;
        //    uint indminver;
        //    magic = Hex2Ascii(BitConverter.ToString(Reader.ReadBytes(4)).Replace("-", "")); // Get the magic number
        //    if (magic == "DBPF")
        //    {
        //        // It's a DBPF file!
        //        // Forget this for now; use only for debugging purposes.  
        //        // MessageBox.Show("DBPF Loaded");
        //        // Header
        //        majver = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                     // major version
        //        minver = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                     // minor version
        //        // NOPE! that's 2.0 only afaik:  Reader.ReadBytes(12);
        //        if (minver == 0)
        //        {
        //            creationdte = BitConverter.ToString(Reader.ReadBytes(4), 0);
        //            modifydte = BitConverter.ToString(Reader.ReadBytes(4), 0);
        //        }
        //        indmajver = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                  // index major version
        //        entrycount = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                 // index entry count
        //        offset = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                     // index offset
        //        size = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                       // index size
        //        tindent = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                    // trash index entry count  
        //        tindoff = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                    // trash index offset
        //        tindsz = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                     // trash index size
        //        indminver = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                  // index minor version
        //        // Wait a minute, aren't there supposed to be 24 reserved bytes before this!? And aren't they the index?
        //        //Reader.ReadBytes(32);                                                                       // 32 bytes of traditional padding

        //        // Now for the index:
        //        // First we find out if there is an index.  There isn't one when indmajver is less than one.
        //        if (indmajver > 1)
        //        {
        //            // Then, do a while loop and create a private integer; or, we could reuse the last integer.
        //            i = 0;
        //            while (i <= (int)entrycount)         // While our integer is NO MORE THAN the entry count:
        //            {
        //                Entry.TypeID = (DBPFTypeID)Reader.ReadInt32();
        //                Entry.GroupID = (DBPFGroupID)Reader.ReadInt32();
        //                Entry.InstanceID = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
        //                Entry.SecondInstanceID = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
        //                Entry.FileOffset = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
        //                Entry.FileSize = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
        //            }
        //            // Index was found, but dump statistics anyway for R&D purposes.
        //            lsbContents.Items.Add("Major version: " + majver);
        //            lsbContents.Items.Add("Minor version: " + minver);
        //            lsbContents.Items.Add("Index major version: " + indmajver + " (index found!)");
        //            lsbContents.Items.Add("Index minor version: " + indminver);
        //            lsbContents.Items.Add("Index entry count: " + entrycount);
        //            lsbContents.Items.Add("Index offset: " + offset);
        //            lsbContents.Items.Add("Index size: " + size);
        //            lsbContents.Items.Add(BitConverter.ToString(Reader.ReadBytes((int)Reader.BaseStream.Length - 96))); // Get the rest of the contents of the file.
        //            Debug.Assert(true, "Index found and statistics dumped.");
        //        }
        //        else
        //        {
        //            // There was no index; so we'll go ahead and check for a DIR.
        //            // The DIR is always 9 bytes large.  This means we'll read 9 bytes altogether.
        //            uint compresssize = 0;                  // File's compressed size
        //            string word = "";                            // Compression ID
        //            byte[] uncompsize = {};                // Uncompressed size
        //            try
        //            {
        //                compresssize = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
        //                word = BitConverter.ToString(Reader.ReadBytes(2));
        //                uncompsize = Reader.ReadBytes(3);
        //            }
        //            catch { }

        //            // There is no index!  Dump statistics.
        //            lsbContents.Items.Add("Major version: " + majver);
        //            lsbContents.Items.Add("Minor version: " + minver);
        //            if (minver == 0)
        //            {
        //                lsbContents.Items.Add("Creation date: " + creationdte);
        //                lsbContents.Items.Add("Modification date: " + modifydte);
        //            }
        //            lsbContents.Items.Add("Index major version: " + indmajver + " (no index)");
        //            lsbContents.Items.Add("Index minor version: " + indminver);
        //            lsbContents.Items.Add("Index entry count: " + entrycount);
        //            lsbContents.Items.Add("Index offset: " + offset);
        //            lsbContents.Items.Add("Index size: " + size);
        //            lsbContents.Items.Add("-DIR-");
        //            lsbContents.Items.Add("Compressed Size " + compresssize.ToString().Replace("-", ""));
        //            lsbContents.Items.Add("Uncompressed Size " + BitConverter.ToString(uncompsize).Replace("-", ""));
        //            lsbContents.Items.Add("Compression ID " + word.Replace("-", ""));
        //            lsbContents.Items.Add(BitConverter.ToString(Reader.ReadBytes((int) Reader.BaseStream.Length - 96))); // Get the rest of the contents of the file.
        //            Debug.Assert(true, "Unable to get index!");

        //        }
        //    }
        //    // Close this reader so the next one can be started.
        //    Reader.Close();
        //    // Now, for debug purposes, we will dump all the hexadecimal contents of the file into the textbox visible when you click the expand button.
        //    // TODO: make this Async.
        //    BinaryReader Reader1 = new BinaryReader(File.Open(txbPath.Text, FileMode.Open));
        //    richTextBox1.Text = BitConverter.ToString(Reader1.ReadBytes((int)Reader1.BaseStream.Length)).Replace("-", "");
        //    richTextBox2.Text = Hex2Ascii(richTextBox1.Text);
        //    lblTitle.Text = "The Sims 2 Package Extractor";
        //    // End of the line.  Stop using the file.
        //    Reader1.Close();
        //    #endregion
    }
}
