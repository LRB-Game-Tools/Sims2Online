﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;

namespace Sims2Files
{
    [RunInstaller(true)]
    public partial class _Installer : System.Configuration.Install.Installer
    {
        public _Installer()
        {
            InitializeComponent();
        }
    }
}
