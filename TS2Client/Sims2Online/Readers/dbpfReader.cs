﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Sims2Files.formats.dbpf;
using System.Diagnostics;
using Sims2Files.formats;
using Sims2Files;
using System.Text;

namespace Sims2Online.Readers
{
    class dbpfReader
    {
        public static DBPFEntry ReadDBPF(String pathToFile)
        {
            #region DBPF Reader
            // Here is where we will load the DBPF file's contents and put them into the listbox.
            BinaryReader Reader = new BinaryReader(File.Open(pathToFile, FileMode.Open));
            DBPFEntry Entry = new DBPFEntry();
            int i = 0;
            string magic = "";
            uint majver;
            uint minver;
            uint indmajver;
            uint entrycount;
            uint offset;
            uint size;
            uint tindent;
            uint tindoff;
            uint tindsz;
            uint indminver;
            /* There are currently a number of issues with this commented code, so I'm going to ignore the magic number for now and override it.
             * magic = Hex2Ascii(BitConverter.ToString(Reader.ReadBytes(4))); // Get the magic number
             */
            #region Override the Magic Number
            Reader.ReadBytes(4);            // Read the bytes anyway
            magic = "DBPF";                    // Make it look like the magic number is DBPF
            #endregion
            if (magic == "DBPF")
            {
                // It's a DBPF file!
                // Forget this for now; use only for debugging purposes.  
                // MessageBox.Show("DBPF Loaded");
                // Header
                majver = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                     // major version
                minver = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                     // minor version
                Reader.ReadBytes(12);                                                                       // these are all set to 0, we can ignore them
                indmajver = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                  // index major version
                entrycount = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                 // index entry count
                offset = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                     // index offset
                size = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                       // index size
                tindent = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                    // trash index entry count  
                tindoff = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                    // trash index offset
                tindsz = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                     // trash index size
                indminver = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);                                  // index minor version
                // Wait a minute, aren't there supposed to be 24 reserved bytes before this!? And aren't they the index?
                //Reader.ReadBytes(32);                                                                       // 32 bytes of traditional padding

                // Now for the index:
                // First we find out if there is an index.  There isn't one when indmajver is less than one.
                if (indmajver > 1)
                {
                    // Then, do a while loop and create a private integer; or, we could reuse the last integer.
                    i = 0;
                    while (i <= (int)entrycount)         // While our integer is NO MORE THAN the entry count:
                    {
                        Entry.TypeID = (DBPFTypeID)Reader.ReadInt32();
                        Entry.GroupID = (DBPFGroupID)Reader.ReadInt32();
                        Entry.InstanceID = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
                        Entry.SecondInstanceID = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
                        Entry.FileOffset = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
                        Entry.FileSize = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
                    }
                    // Index was found, but dump statistics anyway for R&D purposes.
                    Debug.Assert(true, "Major version: " + majver);
                    Debug.Assert(true, "Minor version: " + minver);
                    Debug.Assert(true, "Index major version: " + indmajver + " (index found)");
                    Debug.Assert(true, "Index minor version: " + indminver);
                    Debug.Assert(true, "Index entry count: " + entrycount);
                    Debug.Assert(true, "Index offset: " + offset);
                    Debug.Assert(true, "Index size: " + size);
                    Debug.Assert(true, BitConverter.ToString(Reader.ReadBytes((int)Reader.BaseStream.Length - 96))); // Get the rest of the contents of the file.
                    Debug.Assert(true, "Index found and statistics dumped.");
                }
                else
                {
                    // There is no index!  Dump statistics.
                    Debug.Assert(true, "Major version: " + majver);
                    Debug.Assert(true, "Minor version: " + minver);
                    Debug.Assert(true, "Index major version: " + indmajver + " (no index)");
                    Debug.Assert(true, "Index minor version: " + indminver);
                    Debug.Assert(true, "Index entry count: " + entrycount);
                    Debug.Assert(true, "Index offset: " + offset);
                    Debug.Assert(true, "Index size: " + size);
                    Debug.Assert(true, BitConverter.ToString(Reader.ReadBytes((int)Reader.BaseStream.Length - 96))); // Get the rest of the contents of the file.
                    Debug.Assert(true, "Unable to get index!");
                }
                return Entry;
            }
            else
            {
                return null;
            }
            // End of the line.  Stop using the file.
            Reader.Close();
            #endregion
        }
            
    }
}
