﻿#region Using Statements
using System;
using System.IO;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Security;
using System.Security.Principal;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Threading;
#endregion

namespace Sims2Online
{
    // Compatibility with Linux is still untested!
#if WINDOWS || LINUX || MAC
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            using (var game = new Game1())
            {
                /* Original registry checking code by Mats 'Afr0' Vederhus, modified for The Sims 2. */
                //Controls whether the application is allowed to start.
                bool Exit = false;
                string Software = "";

                if ((Environment.Is64BitOperatingSystem == false) && (Environment.Is64BitProcess == false))
                    Software = "SOFTWARE";
                else
                    Software = "SOFTWARE\\Wow6432Node";

                //Find the path to TSO on the user's system.
                RegistryKey softwareKey = Registry.LocalMachine.OpenSubKey(Software);

                if (Array.Exists(softwareKey.GetSubKeyNames(), delegate(string s) { return s.Equals("EA GAMES", StringComparison.InvariantCultureIgnoreCase); }))
                {
                    RegistryKey eaKey = softwareKey.OpenSubKey("EA GAMES");
                    if (Array.Exists(eaKey.GetSubKeyNames(), delegate(string s) { return s.Equals("The Sims 2", StringComparison.InvariantCultureIgnoreCase); }))
                    {
                        RegistryKey ts2Key = eaKey.OpenSubKey("The Sims 2");
                        string installDir = (string)ts2Key.GetValue("Install Dir Parent");
                        if (ts2Key.GetValue("DisplayName").ToString().ToLower().Contains("ultimate collection"))
                        {
                            installDir += "\\..\\";
                            game.strPath = installDir;
                        }
                        else
                        {
                            MessageBox.Show("Error: The Sims 2 was installed, but wasn't the Ultimate Collection.");
                            Exit = true;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Error: The Sims 2 Ultimate Collection was not found on your system.");
                        Exit = true;
                    }
                }
                else
                {
                    MessageBox.Show("Error: No EA products were found on your system.");
                    Exit = true;
                }

                if (!Exit)
                {
                    game.Run();
                }
                for (int i = 0; i < args.Length; i++)
                {
                    string arg_ = args[i];
                    if (arg_.StartsWith("-") && arg_.Contains("x"))
                    {
                        // probably a resolution, try formatting it
                        try
                        {
                            arg_ = arg_.ToLower();
                            string parsedX;
                            string parsedY = "";
                            parsedX = arg_.Remove(arg_.IndexOf("x"), arg_.Length - arg_.IndexOf("x") - 1);
                            game.strH = arg_;
                        }
                        finally { }
                    }
                }
            }
                

        }
    }
#endif
}
