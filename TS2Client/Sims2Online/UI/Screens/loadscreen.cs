﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using ObjImporter;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;
using Sims2Files.formats.dbpf;
using Sims2Files;
using Sims2Content;

namespace Sims2Online.UI.Screens
{
    class loadscreen
    {
        #region Variables
        // Window properties
        public static int winwidth = 1280;
        public static int winheight = 720;

        // Loading text properties
        //     Strings
        public static String[] loadingTextList = { "Custom loading text anyone?", "It goes with the tradition of new text every expansion pack...", "String 3! 'ello again!", "Go grab a coffee!", "Don't make me flip..." };
        public static string Loadingtesttext;
        public static string strLastString;
        //     Integers
        public static int loadingRound = 0;
        //     Booleans
        public static bool needsToChange = false;
        //     Positions and Sizes
        public static GameTime gametime1 = new GameTime();
        public static Vector2 FontPos = new Vector2(x, y);
        //     Fonts
        public static SpriteFont Font_Def;

        // Label properties
        public static float x = winwidth;
        public static float y = 0;

        // Textedit controls
        public static UI.Elements.UITextBox textedit;

        // Background 
        //     Texture
        public static Texture2D backtex;
        #endregion
        public static void Init(SpriteBatch spb, Game1 game)
        {
            try
            {
                // Startup strings:
                // FILE: Double Deluxe\SP4\TSData\Res\Text\UIText.package
                // INSTANCE: 0x000000DC
                // TYPE: STR#
                // INSTANCE (HIGH): 0x00000000
                // GROUP: 0xFFFFFFFF
                // COMPRESSED: true
                // STRINGS: 0x0000 to 0x0009
                y = spb.GraphicsDevice.PresentationParameters.BackBufferHeight - 20;
                winwidth = spb.GraphicsDevice.PresentationParameters.BackBufferWidth;
                winheight = spb.GraphicsDevice.PresentationParameters.BackBufferHeight;
                // load the image into the memory @backtex variable
                Stream stream1 = new FileStream("./titlebackground.png", FileMode.Open);
                backtex = Texture2D.FromStream(spb.GraphicsDevice, stream1);
                // draw backtex
                spb.Draw(backtex, new Microsoft.Xna.Framework.Rectangle(0, 0, winwidth, winheight), Microsoft.Xna.Framework.Color.White);
                // Close the reader before the stream to ensure a safe & successful closure.
                stream1.Close();
            }
            catch { } 
        }

        public static void Update(SpriteBatch spb, Game1 game)
        {
            try
            {
                spb.Draw(backtex, new Microsoft.Xna.Framework.Rectangle(0, 0, winwidth, winheight), Microsoft.Xna.Framework.Color.White);

                if (Loadingtesttext == null)
                {
                    needsToChange = true;
                    Loadingtesttext = loadingTextList[loadingRound];
                }
                if (x <= 0 - (Game1.Font1.MeasureString(Loadingtesttext).X / 2))
                {
                    // Available loading text variables:
                    //  - loadingTextList[loadingRound]
                    //  - Loadingtesttext
                    x = winwidth + Game1.Font1.MeasureString(Loadingtesttext).X / 2;
                    loadingRound++;
                    if (needsToChange == true)
                    {
                        Loadingtesttext = loadingTextList[loadingRound];
                        strLastString = Loadingtesttext;
                    }
                }
                x = x - 10;
                Vector2 FontOrigin = Game1.Font1.MeasureString(Loadingtesttext) / 2;
                FontPos = new Vector2(x, y);
                if (Loadingtesttext.ToLower().Contains("flip"))
                {
                    spb.DrawString(Game1.Font1, Loadingtesttext, FontPos, Microsoft.Xna.Framework.Color.White, 0, FontOrigin, 1.0f, SpriteEffects.FlipHorizontally, 0.5f);
                }
                else
                {
                    spb.DrawString(Game1.Font1, Loadingtesttext, FontPos, Microsoft.Xna.Framework.Color.White, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
                }
                if (loadingRound > loadingTextList.Length - 1)               // Loading sequence *appears* to have finished.
                {
                    GameController.strGameScreen = "login";    // We're finished here, set the game screen to login.
                }
            }
            catch { } 
        }
    }
}
