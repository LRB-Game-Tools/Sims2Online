﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using ObjImporter;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;
using Sims2Files.formats.dbpf;
using Sims2Files;
using Sims2Content;

namespace Sims2Online.UI.Screens
{
    class loginscreen
    {
        #region Variables
        // Background
        //     Texture
        public static Texture2D backtex;

        // Window properties
        public static int winwidth = 1280;
        public static int winheight = 720;

        // Data
        Byte[] decompressed;
        public static string output;
        #endregion
        public static void Init(SpriteBatch spb)
        {
            try
            {
                Vector2 FontPos = new Vector2(10, 10);
                Vector2 FontOrigin = Game1.Font1.MeasureString("Login:") / 2;
                winwidth = spb.GraphicsDevice.PresentationParameters.BackBufferWidth;
                winheight = spb.GraphicsDevice.PresentationParameters.BackBufferHeight;
                // Since loadscreen's backtex is a static variable, we may as well just use that!
                backtex = loadscreen.backtex;
                if (backtex == null)
                {
                    Stream stream1 = new FileStream("./titlebackground.png", FileMode.Open);
                    backtex = Texture2D.FromStream(spb.GraphicsDevice, stream1);
                    stream1.Close();
                }
                spb.Draw(backtex, new Microsoft.Xna.Framework.Rectangle(0, 0, winwidth, winheight), Microsoft.Xna.Framework.Color.White);
                spb.DrawString(Game1.Font1, "Login:", new Vector2(10, 10), Microsoft.Xna.Framework.Color.White, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
                Package pkage = new Package(@"D:\Origin Games\The Sims 2 Ultimate Collection\Fun with Pets\SP9\TSData\Res\UI\ui.package"); 
                output = pkage.Read(0x00007EEC, 0x0000006A, pkage.file_);
                
            }
            catch
            {
                // Retry initializing.
                Init(spb);
            }
        }
        public static void Update(SpriteBatch spb, Game1 game)
        {
            try
            {
                Vector2 FontPos = new Vector2(10, 10);
                Vector2 FontOrigin = Game1.Font1.MeasureString("Login:") / 2;
                spb.Draw(backtex, new Microsoft.Xna.Framework.Rectangle(0, 0, winwidth, winheight), Microsoft.Xna.Framework.Color.White);
                spb.DrawString(Game1.Font1, "Login:", new Vector2(10,10), Microsoft.Xna.Framework.Color.White, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
                spb.DrawString(Game1.Font1, output, new Vector2(100, 100), Microsoft.Xna.Framework.Color.White, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
            }
            catch { }
        }
    }
}
