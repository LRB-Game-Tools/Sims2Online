﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Sims2Files;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.GamerServices;

namespace Sims2Online
{
    class GameController
    {
        public static string strGameScreen = "loading";
        public static Boolean hasInit = false;
        public static void InitGame(SpriteBatch spb, Game1 game)
        {
            //objTesting.Init(spb); // start the object tester
            //Forget that - go to login instead
            if (strGameScreen == "loading")
            {
                UI.Screens.loadscreen.Init(spb, game);
                //BinaryReader stream_m = new BinaryReader(File.Open(game.strPath + @"Double Deluxe\Base\TSData\Res\Sound\Splash.package", FileMode.Open));
                //Byte[] contents;
                //stream_m.ReadBytes((int)Sims2Online.FileLocations._locs.filelocs.splash1);
                //contents = stream_m.ReadBytes((int)0x001464cb);
                //stream_m.Close();
                //Sims2Files.XA.XAFile xaSplash = new Sims2Files.XA.XAFile(contents);
                //xaSplash.LoadFile(contents);
                //SoundEffect soundEffect;
                //xaSplash.DecompressFile();
                //StreamWriter strw = new StreamWriter(File.Open(@"C:\test.wav", FileMode.Create));
                //strw.Write(xaSplash.DecompressedData);
                //strw.Close();
                //soundEffect = SoundEffect.FromStream(xaSplash.DecompressedStream);
                //soundEffect.Play();
            }
            if (strGameScreen == "login")
            {
                UI.Screens.loginscreen.Init(spb);
            }
        }

        public static void UpdateGame(SpriteBatch spb, GameTime gameTime, Game1 game)
        {
            Game1.m_FPS = (float)(1 / gameTime.ElapsedGameTime.TotalSeconds);

            if (strGameScreen == "loading")
            {
                UI.Screens.loadscreen.Update(spb, game);
            }
            if (strGameScreen == "login")
            {
                if (!hasInit) UI.Screens.loginscreen.Init(spb); hasInit = true;
                UI.Screens.loginscreen.Update(spb, game);
            }
        }
    }
}
