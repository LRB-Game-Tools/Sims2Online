﻿#region Using Statements
using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Audio;
using Sims2Online;
using Sims2Files;
#endregion

namespace Sims2Online
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    /// 
    public class Game1 : Game
    {
        public static SpriteFont Font1;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Vector2 FontPos;
        public static float m_FPS = 0;
        public static bool doneOnce = false;
        public bool blnFullscreen;
        public int intHeight = 720;
        public int intWidth = 1280;
        public String strH;
        public String strPath = "";

        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;
            graphics.PreferredBackBufferFormat = SurfaceFormat.Vector4;
            graphics.PreferMultiSampling = true;
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            Font1 = Content.Load<SpriteFont>("Arial");
            FontPos = new Vector2(42, 10);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.LeftAlt) && Keyboard.GetState().IsKeyDown(Keys.F4))
                Exit();

            // TODO: Add your update logic here
            base.Update(gameTime);
            try
            {
                spriteBatch.Begin();
                GameController.UpdateGame(spriteBatch, gameTime, this);
                spriteBatch.End();
            }
            catch { }
        }

        protected override void Draw(GameTime gameTime)
        {
            // Draw everything here.
            SpriteBatch spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteBatch.Begin();
            if (!doneOnce)
            {
                GameController.InitGame(spriteBatch, this);
                doneOnce = !doneOnce;
            }
            GameController.UpdateGame(spriteBatch, gameTime, this);
            try
            {
                Vector2 FontOrigin = Font1.MeasureString(m_FPS.ToString()) / 2;
                spriteBatch.DrawString(Font1, GameController.strGameScreen, new Vector2(graphics.PreferredBackBufferWidth - Font1.MeasureString(GameController.strGameScreen).X, 15), Color.LightGreen, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
                
                spriteBatch.DrawString(Font1, ((int)m_FPS).ToString() + " fps", FontPos, Color.LightGreen, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
                /* Needs further optimisation and decompression!
                 * Package pkage = new Package(@"D:\Origin Games\The Sims 2 Ultimate Collection\Fun with Pets\SP9\TSData\Res\UI\ui.package"); 
                 * String output = pkage.Read(0x00007EEC, 0x0000006A, pkage.file_);
                 * spriteBatch.DrawString(Game1.Font1, output, new Vector2(100, 100), Microsoft.Xna.Framework.Color.White, 0, FontOrigin, 1.0f, SpriteEffects.None, 0.5f);
                */
            }
            catch { }
            spriteBatch.End();
            base.Draw(gameTime);

        }

        public void Quit()
        {
            this.Exit();
        }
    }
}
