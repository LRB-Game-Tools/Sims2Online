# Sims2Online
Experimental side project.  Written in XNA &amp; C#.  Aims to have an online capability.
Research is currently underway and we will have working code hopefully within the next 6 months - 1 year.  

## What is Sims2Online?
Sims2Online is a repository containing the code for The Sims 2 Online - an online "version" of The Sims 2.
It will not come with any of The Sims 2's original files; they will be read from the user's installation at runtime.

## Requirements
Here are the requirements that have been projected for the final product; however, as the project is so far off of completion, these figures are not trustworthy and are purely estimates:

* 2GB+ RAM

* 50-100MB free storage space (for compiled binaries, up to 1-2GB for source code)

* nVidia 6600M / AMD counterpart and up

* The Sims 2 Complete Collection

* 1Mbps upload and download speed

* Pentium G3258 / AMD counterpart and up

*This application is known to require upwards of a 1366x768 resolution as the window size has been hard-coded to 720p.  This ensures that the game experience is rounded and in the future other resolutions will be available.*

## Why The Sims 2 Ultimate Collection?
This way we can ensure that all of our users have the same objects and can make sure that all objects work before the game is completed; however, some of the objects may be inaccessible in the development stage.

## How to get The Sims 2 Ultimate Collection
First, get your Sims 2 key from your Sims 2 CD - it does not matter what edition you have, as long as it is The Sims 2.  Then, either activate it on Origin or ask Origin Support to activate it for you (because sometimes, the former does not work).  You will then find The Sims 2 Ultimate Collection in your library and can download it (10GB download size, 12GB extracted).  This is one of the easiest methods of getting all the expansion packs required, but you can also try buying from online stores such as EBay, Amazon, or your preferred store.  **As long as ALL expansion packs are installed, you will be able to run Sims2Online**.

## Extra information
There will be extra objects included in the game to enhance your playing experience, for example minigames that you can play with friends.  However, these will aim to keep the original Maxis style that is shown throughout the game.
### Release dates
A tech demo aims to be released at the end of the year - watch this space!


