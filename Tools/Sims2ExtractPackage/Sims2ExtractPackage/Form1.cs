﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Sims2Files;
using Sims2Files.utils;
using Sims2Files.formats;
using Sims2Files.formats.dbpf;
#endregion
namespace Sims2ExtractPackage
{
    public partial class frmMainForm : Form
    {
        public frmMainForm()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Loading..."; lblTitle.Refresh();  // Set the title text and refresh
            lsbContents.Items.Clear();  // Clear the items before we begin
            DBPF dbpfFile = new DBPF(txbPath.Text);
            DBPFEntry dbpfFileEntry = new DBPFEntry(); 
            try { foreach (KeyValuePair<uint, byte[]> item in dbpfFile.GetItemsByType(DBPFTypeID.UI)) {
                    lsbContents.Items.Add(item.Key);
                    lsbContents.Items.Add(item.Value);
                }} catch (Exception ex) { lsbContents.Items.Add("Error: " + ex.ToString()); }
            try { KeyValuePair<uint, Byte[]> testentry = dbpfFile.GetItemsByType(DBPFTypeID.UI)[1];
                  lsbContents.Items.Add(testentry);
                } catch (Exception ex) { lsbContents.Items.Add("Error: " + ex.ToString()); }
            try { lsbContents.Items.Add(Hex2Ascii(BitConverter.ToString(dbpfFile.GetItemByID(0x00000000)))); }
            catch (Exception ex) { lsbContents.Items.Add("Error: " + ex.ToString()); }
            // ----------------------------
            // Test UI Data file info
            // ----------------------------
            // Index info:
            // ----------------------------
            // Index type: ptShortFileIndex
            // Version: 0x0000000100000001
            // Hole index empty
            // ----------------------------
            // File info:
            // ----------------------------
            // Instance ID: 0x49060F07
            // Instance (high): 0x00000000
            // Group: 0xA99D8A11
            // Type: UI (0x00000000)
            // Compressed: NO! :D
            // Size: 0x000014cf
            // ----------------------------
            Package pkg = new Package(txbPath.Text);
            richTextBox1.Text = pkg.Read(0x00007EEC, 0x0000006A, pkg.file_);
            richTextBox2.Text = pkg.Read(1, pkg.file_);
            MessageBox.Show(pkg.numFiles.ToString());
            int i = 1;
            while (i < pkg.numFiles)
            {
                lsbContents.Items.Add("File " + i.ToString());
            }
            this.lblTitle.Text = "The Sims 2 Package Extractor";
            // ----------------------------
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.Size.Width == 1000)
            {
                button1.Text = ">";
                this.Size = new Size(507, this.Size.Height);
            }
            else
            {
                button1.Text = "<";
                this.Size = new Size(1000, this.Size.Height);
            }
        }

        public static string Hex2Ascii(string hexString)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hexString.Length; i += 2 /* Hex values always come in twos! Therefore it's +2. */)
            {
                if(hexString.Substring(i, 2) != "00")   
                {

                    /* The "00" substring is always considered "." in
                     * most Hex editors but we'll go for a simple dash 
                     * instead to avoid confusion (seen below in the 
                     * else statement!). */

                    string hs = hexString.Substring(i, 2);
                    sb.Append(Convert.ToString(Convert.ToChar(Int32.Parse(hexString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber))));
                }
                else
                {
                    sb.Append("-");
                }
            }
            return sb.ToString();
        }

        private void lblTitle_Click(object sender, EventArgs e)
        {
            lblTitle.Text = "Coded by LetsRaceBwoi";
        }

        private void lsbContents_SelectedIndexChanged(object sender, EventArgs e)
        {
#if DEBUG
            if (lsbContents.SelectedItem.ToString().ToLower().Contains("error: ")) { MessageBox.Show(lsbContents.SelectedItem.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1); }
#endif
        }

        private void frmMainForm_Load(object sender, EventArgs e)
        {

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = "Open Package File";
            ofd.Filter = "Package File|*.package|All Files|*.*";
            ofd.FileName = txbPath.Text;
            ofd.ShowDialog();
            try
            {
                txbPath.Text = ofd.FileName;
            }
            catch
            {

            }
        }
    }
}
