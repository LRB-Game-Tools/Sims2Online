﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
    

namespace Sims2Server
{
    class Program
    {
        static GUI myForm;
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool AllocConsole();
        [STAThread]
        public static void Main(string[] args)
        {
            Boolean guiSet = false;
            foreach (string arg in args)
            {
                if (arg == "-gui")
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    myForm = new GUI();
                    Application.Run(myForm);
                    _main.init(args);
                    _main.gui = true;
                    guiSet = true;
                }
                if (arg == "-cli")
                {
                    AllocConsole();    // Allocate us a console!
                    _main.init(args);
                    _main.gui = false;
                    guiSet = true;
                }
            }
            if (!guiSet)
            {
                // Default : Gui
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                myForm = new GUI();
                Application.Run(myForm);
                _main.init(args);
                _main.gui = true;
                guiSet = true;
            }
        }
    }
}
