﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sims2Server
{
    class PacketIDs
    {
        public enum PacketTypes : uint
        {
            LOGIN_REQUEST = 0x01,
            LOGIN_SUCCESS = 0x02,
            LOGIN_FAIL = 0x03,
            CHALLENGE_RESPONSE = 0x04,
            USER_LIST = 0x05,
            CHARACTER_CREATE = 0x06,
            CHARACTER_DELETE = 0x07,
            CHARACTER_CREATE_FAIL = 0x08,
            REQUEST_GUID = 0x09,
            NBHOOD_LIST = 0x10,
            JOIN_NBH = 0x11
        }
        public static string PacketDescriptors(PacketTypes pt)                                                                             // This should not be called, but if it is return the requested item.
        {
                String LOGIN_REQUEST = "0x01 - Request login";
                String LOGIN_SUCCESS = "0x02 - Login was a success";
                String LOGIN_FAIL = "0x03 - Login failed";
                String CHALLENGE_RESPONSE = "0x04 - Challenge response";
                String USER_LIST = "0x05 - Request / send a list of online users";
                String CHARACTER_CREATE = "0x06 - Create a character";
                String CHARACTER_DELETE = "0x07 - Delete a character";
                String CHARACTER_CREATE_FAIL = "0x08 - Character creation failed";
                String REQUEST_GUID = "0x09 - Request a GUID specific to a session";
                String NBHOOD_LIST = "0x10 - Request a neigbourhood list";
                String JOIN_NBH = "0x11 - Join a neighbourhood list";
            
            
            if(pt == PacketTypes.LOGIN_REQUEST)
                return (LOGIN_REQUEST);
            else if(pt == PacketTypes.LOGIN_SUCCESS)
                return (LOGIN_SUCCESS);
            else if(pt == PacketTypes.LOGIN_FAIL)
                return (LOGIN_FAIL);
            else if(pt == PacketTypes.CHALLENGE_RESPONSE)
                return (CHALLENGE_RESPONSE);
            else if(pt == PacketTypes.USER_LIST)
                return (USER_LIST);
            else if(pt == PacketTypes.CHARACTER_CREATE)
                return (CHARACTER_CREATE);
            else if(pt == PacketTypes.CHARACTER_DELETE)
                return (CHARACTER_DELETE);
            else if(pt == PacketTypes.CHARACTER_CREATE_FAIL)
                return (CHARACTER_CREATE_FAIL);
            else if(pt == PacketTypes.REQUEST_GUID)
                return (REQUEST_GUID);
            else if(pt == PacketTypes.NBHOOD_LIST)
                return (NBHOOD_LIST);
            else if(pt == PacketTypes.JOIN_NBH)
                return (JOIN_NBH);
            else
                return null;                                                // If this happens something is wrong!
        }
    }
}
