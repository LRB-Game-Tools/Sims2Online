﻿/* ============================================ [The Sims 2 Online server] =============================================
 * This should NOT require any Sims2Files libraries so it can run easily and quickly on servers with low specifications. 
 * This software is designed for servers with <=2GB RAM and will keep it's usage low.  It can be run on any processor, 
 * be it Pentium or Xeon.  Higher-spec servers can hold more players but lower-spec servers will still be able to fit
 * a lot of players online.
 * =========================================================================================================== */
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Security.Cryptography;
using System.IO;
using GonzoNet.Encryption;
using GonzoNet;
using Microsoft.Win32;

namespace Sims2Server
{
    class _main
    {
        private static void Logger_OnMessageLogged(LogMessage Msg)
        {
            Console.WriteLine("Gonzo: " + Msg.Message);
        }
        
        private static void m_Client_OnConnected(LoginArgsContainer LoginArgs)
        {
            PacketSenders.SendInitialConnectPacket(LoginArgs.Client, LoginArgs.Username);
            Console.WriteLine("Sent first packet!\r\n");
        }

        static void Main(string[] args)
        {
            String isClient = "";
            try
            {
                isClient = args[0];
            }
            catch
            {
                isClient = "false";
            }
            #region GonzoNet Initialization (Server)
            if (isClient.Equals("false"))
            {
                GonzoNet.PacketHandlers.Register(0x01, false, 0, new OnPacketReceive(PacketHandlers.OnLoginRequest));
                GonzoNet.PacketHandlers.Register(0x02, false, 0, new OnPacketReceive(PacketHandlers.OnLoginSuccess));
                GonzoNet.PacketHandlers.Register(0x04, false, 0, new OnPacketReceive(PacketHandlers.OnChallengeRecieved));
                NetworkFacade.Listener.Initialize(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 2223));
            }
            #endregion
            #region Information
            Console.SetWindowSize(120, 45);
            string externalip = new WebClient().DownloadString("http://icanhazip.com");    
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Welcome to the ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("TS2 Online ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Server application!\n");                                      // Welcome text
            Console.WriteLine("IP Address: " + externalip);                 // IP Address
            Console.SetCursorPosition(0, Console.CursorTop - 1);
            Console.WriteLine("Version: " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            String command = "";
            Console.ForegroundColor = ConsoleColor.Yellow;
            #endregion
            #region Commands

            while (!command.ToLower().Equals("shutdown"))
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write("[ ");
                Console.ForegroundColor = ConsoleColor.White;
                command = Console.ReadLine();
                Console.CursorLeft = 0;
                Console.SetCursorPosition(0, Console.CursorTop - 1);
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.Write("[ " + command + " ]");
                Console.SetCursorPosition(0, Console.CursorTop + 1);
                Console.ForegroundColor = ConsoleColor.Yellow;
                // Now read the command.
                if (command.ToLower().Equals("help"))
                {
                    Console.WriteLine("Help: Commands:");
                    int i = 0;
                    try
                    {
                        while (_commands.commands[i] != null)
                        {
                            Console.WriteLine(_commands.commands[i]);
                            i++;
                        }
                    }
                    catch { }       // I'm not going to do anything here so there's no point wasting a couple of lines for some useless brackets.
                    
                }

                else if (command.ToLower().Contains("forcekill"))
                {
                    string ipaddress = command.Replace("forcekill ", "");
                    try
                    {
                        // Shutdown the client here
                        Console.WriteLine("Client shutdown.");
                    }
                    catch
                    {
                        // Show an error message here - was the IP address connected?
                    }
                }

                else if (command.ToLower().Contains("logoff"))
                {
                    string ipaddress = command.Replace("logoff ", "");
                    try
                    {
                        // Disconnect the client here
                        Console.WriteLine("Client disconnected.");
                    }
                    catch
                    {
                        // Show an error message here - was the IP address connected?  Was it a GUID instead of an IP address?
                    }
                }
                    
                else if (command.ToLower().Contains("getpacketids"))
                {
                    foreach (PacketIDs.PacketTypes packetName in Enum.GetValues(typeof(PacketIDs.PacketTypes)))
                    {
                        Console.WriteLine(packetName);
                        Console.WriteLine("\t" + PacketIDs.PacketDescriptors(packetName) + "\n");
                    }
                }
                else if (command.ToLower().Contains("sendtest"))
                {
                    //GonzoNet requires a log output stream to function correctly. This is built in behavior.
                    GonzoNet.Logger.OnMessageLogged += new MessageLoggedDelegate(Logger_OnMessageLogged);

                    NetworkFacade.Client = new NetworkClient("127.0.0.1", 2223, GonzoNet.Encryption.EncryptionMode.NoEncryption, false);
                    NetworkFacade.Client.OnConnected += new OnConnectedDelegate(m_Client_OnConnected);
                    
                    LoginArgsContainer LoginArgs = new LoginArgsContainer();
                    LoginArgs.Enc = new AESEncryptor("test");
                    LoginArgs.Username = command.ToLower().Replace("sendtest ", "");
                    LoginArgs.Password = "test";
                    LoginArgs.Client = NetworkFacade.Client;

                    SaltedHash Hash = new SaltedHash(new SHA512Managed(), LoginArgs.Username.Length);
                    PacketHandlers.PasswordHash = Hash.ComputePasswordHash(LoginArgs.Username, LoginArgs.Password);

                    NetworkFacade.Client.Connect(LoginArgs);
                    NetworkFacade.Client.Disconnect();
                }
                //Put all new commands above here.
                else if (command.ToLower().Contains("shutdown"))
                {
                    //Having nothing here should make it shut down, in theory.
                }
                else
                {
                    Console.WriteLine("Command " + command + @" not recognized; type ""help"" to get the current list of commands");
                    command = "";
                }
            #endregion
            }

        }
    }
}
