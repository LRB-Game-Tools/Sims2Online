﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sims2Server
{
    class _commands
    {
        public static List<String> commands = new List<string>()
        {
            // This is where the help text for commands will be stored.
            "USAGE - FORCEKILL [IPAddress]  |  Force a user's client to shut down.",
            "USAGE - LOGOFF [IPAddress] -OR- LOGOFF [GUID]  |  Force a client to be logged off",
            "USAGE - SHUTDOWN  |  Shutdown the server",
            "USAGE - GETPACKETIDS  |  Get all the packets that can be sent or recieved along with their descriptions",
            //"USAGE - "
        };
    }
}
