﻿/* This is an INI loader that will load all of the config variables for the Sims2Online server.
 * Here is an example of a valid config INI:
 * 
 * [Sims2OnlineServer.Config]
 * Packets.Skippable = false;
 * Packets.GetHelp = false;
 * Commands.GetHelp = true;
 * MOTD.Set = false;
 * MOTD.Title = "Welcome!";
 * MOTD.Description =
 *   "Welcome! Welcome to City 17!\n
 *    You have chosen, or been chosen\n
 *    to relocate to one of our finest\n
 *    remaining urban centers.";
 * MOTD.Date = 31/12/1969;
 * MOTD.Visible = true;
 * MOTD.OnFirstLaunchOnly = true;
 */


//===================================================THIS IS OBSOLETE! I'll be using GlobalSettings from now on!==========================================
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Sims2Server
{
    class LoadINI
    {
        public static string GetINIContents(String path, String variableName)
        {
            Dictionary<string, string> INIVars = new Dictionary<string, string>();
            String fileContents = "";
            Boolean PacketsSkippable = true;
            Boolean PacketsGetHelp = true;
            Boolean MOTDSet = true;
            String MOTDTitle = "";
            String MOTDDescription = "";
            String MOTDDate = "";
            Boolean MOTDVisible = true;
            Boolean MOTDOnFirstLaunchOnly = false;
            String currentLine = "";

            // Get the INI contents;
            StreamReader iniFile = new StreamReader(path);
            fileContents = iniFile.ReadToEnd();
            iniFile.Close();
            StringReader stringReader1 = new StringReader(fileContents);
            currentLine = stringReader1.ReadLine();
            while (!currentLine.Equals(null))
            {
                if (currentLine.ToLower().Contains("packets.skippable"))
                    PacketsSkippable = Convert.ToBoolean(currentLine.Remove(currentLine.ToLower().IndexOf("packets.skippable = "), currentLine.Length - currentLine.ToLower().IndexOf("packets.skippable = ")));
                if (currentLine.ToLower().Contains("packets.gethelp"))
                    PacketsGetHelp = Convert.ToBoolean(currentLine.Remove(currentLine.ToLower().IndexOf("packets.gethelp = "), currentLine.Length - currentLine.ToLower().IndexOf("packets.gethelp = ")));
                if (currentLine.ToLower().Contains("motd.set"))
                    MOTDSet = Convert.ToBoolean(currentLine.Remove(currentLine.ToLower().IndexOf("motd.set = "), currentLine.Length - currentLine.ToLower().IndexOf("motd.set = ")));
                if (currentLine.ToLower().Contains("motd.title"))
                    MOTDTitle = currentLine.Remove(currentLine.ToLower().IndexOf("packets.skippable = "), currentLine.Length - currentLine.ToLower().IndexOf("packets.skippable = "));
                if (currentLine.ToLower().Contains("motd.description"))
                    MOTDDescription = currentLine.Remove(currentLine.ToLower().IndexOf("packets.skippable = "), currentLine.Length - currentLine.ToLower().IndexOf("packets.skippable = "));
                if (currentLine.ToLower().Contains("motd.date"))
                    MOTDDate = currentLine.Remove(currentLine.ToLower().IndexOf("packets.skippable = "), currentLine.Length - currentLine.ToLower().IndexOf("packets.skippable = "));
                if (currentLine.ToLower().Contains("motd.visible"))
                    MOTDVisible = Convert.ToBoolean(currentLine.Remove(currentLine.ToLower().IndexOf("motd.visible = "), currentLine.Length - currentLine.ToLower().IndexOf("motd.visible = ")));
                if (currentLine.ToLower().Contains("motd.set"))
                    MOTDOnFirstLaunchOnly = Convert.ToBoolean(currentLine.Remove(currentLine.ToLower().IndexOf("motd.onfirstlaunchonly = "), currentLine.Length - currentLine.ToLower().IndexOf("motd.onfirstlaunchonly = ")));
            }
            INIVars.Add("PacketsSkippable", PacketsSkippable.ToString());
            INIVars.Add("PacketsGetHelp", PacketsGetHelp.ToString());
            INIVars.Add("MOTDSet", MOTDSet.ToString());
            INIVars.Add("MOTDTitle", MOTDTitle);
            INIVars.Add("MOTDDescription", MOTDDescription);
            INIVars.Add("MOTDDate", MOTDDate);
            INIVars.Add("MOTDVisible", MOTDVisible.ToString());
            INIVars.Add("MOTDOnFirstLaunchOnly", MOTDOnFirstLaunchOnly.ToString());
            try
            {
                return INIVars[variableName];
            }
            catch
            {
                return null;
            }
        }
    }
}